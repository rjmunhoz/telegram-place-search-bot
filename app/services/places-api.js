'use strict'

const axios = require('axios')

class PlacesApiService {
  constructor (config) {
    this.$http = axios.create({
      baseURL: 'https://maps.googleapis.com/maps/api/place',
      params: {
        key: config.google.places.API_TOKEN
      }
    })
  }

  async textSearch (query) {
    return this.$http.get('/textsearch/json', {
      params: {
        query
      }
    })
    .then(response => response.data.results)
  }
}

module.exports = PlacesApiService
