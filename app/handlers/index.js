'use strict'

module.exports = {
  'inline-query-result': require('./inline-query-result')
}

module.exports.responseTypes = {
  inlineQueryResult: 'inline-query-result'
}
