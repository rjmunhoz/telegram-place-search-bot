'use strict'

const handle = (msg, bot, { results, options = {} }) => {
  return bot.answerInlineQuery(msg.id, results, options)
}

module.exports = { handle }
