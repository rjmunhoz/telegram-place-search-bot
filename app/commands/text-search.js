'use strict'

const { responseTypes } = require('../handlers')

const createResult = place => {
  return {
    type: 'venue',
    id: place.place_id,
    latitude: place.geometry.location.lat,
    longitude: place.geometry.location.lng,
    title: place.name,
    address: place.formatted_address,
    thumb_url: place.icon
  }
}

const factory = (bot, placesService) => async msg => {
  const places = await placesService.textSearch(msg.query)

  const results = places.map(createResult)

  return [{
    type: responseTypes.inlineQueryResult,
    results
  }]
}

module.exports = { factory }
