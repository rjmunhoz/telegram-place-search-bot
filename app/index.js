'use strict'

const { format } = require('util')
const commands = require('./commands')
const handlers = require('./handlers')
const TelegramBot = require('tgfancy')
const PlacesService = require('./services/places-api')
const debug = require('debug')('location-search-bot:main')

const MSG = {
  CODE: '```\n%s```',
  ERR_NO_HANDLER: 'No handler registered for response type "%s"'
}

class App {
  constructor (config) {
    this.$bot = new TelegramBot(config.telegram.API_TOKEN, {
      polling: {
        autoStart: false
      }
    })

    this.$services = {
      places: new PlacesService(config)
    }

    this.$commands = {
      textSearch: commands.textSearch.factory(this.$bot, this.$services.places)
    }

    this.$bot.on('inline_query', this.handleCommand(this.$commands.textSearch))
  }

  start () {
    this.$bot.startPolling()
    this.$bot.getMe()
             .then(me => {
               debug(`Listening on ${me.username}`)
             })
  }

  handleCommand (command) {
    return async msg => {
      const results = await command(msg)

      if (!results) {
        return
      }

      for (const result of results) {
        if (!(result.type in handlers)) {
          const text = format(MSG.ERR_NO_HANDLER, result.type)
          return this.handleError(new Error(text), msg)
        }

        await handlers[result.type].handle(msg, this.$bot, result)
        .catch(err => {
          return this.handleError(err, msg)
        })
      }
    }
  }

  handleError (err, msg) {
    if (msg.query) {
      console.error(err)
      return this.$bot.answerInlineQuery(msg.id, [])
    }

    this.$bot.sendMessage(msg.chat.id, format(MSG.CODE, err.message, {
      parse_mode: 'Markdown'
    }))
  }
}

module.exports = App
