'use strict'

const env = require('sugar-env')

module.exports = {
  telegram: {
    API_TOKEN: env.get('TELEGRAM_API_TOKEN')
  },
  google: {
    places: {
      API_TOKEN: env.get('GOOGLE_PLACES_API_TOKEN')
    }
  }
}
